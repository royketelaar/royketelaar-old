// This is where it all goes :)
$(document).ready(function () {
  $('.filter-buttons ul li').click(function(){
    $('.filter-buttons ul li').removeClass('active');
    $(this).addClass('active');

    var data = $(this).attr('data-filter');
    $grid.isotope({
      filter: data
    })
  });

  var $grid = $(".project-list").isotope({
    itemSelector: ".project",
    percentPosition: true,
    masonry: {
      columnWidth: ".project",
      // fitWidth: true,
      gutter: 16,
    }
  })
  // Adding space after comma
  $(".roles").text(function(i, val) {
      return val.replace(/,/g, ", ");
  });
})


